/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol_examen;

/**
 *
 * @author ameri
 */
public class Arbol_Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario_Examen miArbol = new ArbolBinario_Examen();
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(68, "D");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(71, "G");
        miArbol.agregarNodo(77, "M");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(85, "U");
        miArbol.agregarNodo(69, "E");
        miArbol.agregarNodo(76, "L");
        miArbol.agregarNodo(89, "Y");
        miArbol.agregarNodo(84, "T");
        miArbol.agregarNodo(66, "B");

        
        System.out.println("InOrden");
        if (!miArbol.estaVacio()){
            miArbol.inOrden(miArbol.raiz);
        }
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        System.out.println("PostOrden");
        if (!miArbol.estaVacio()){
            miArbol.postOrden(miArbol.raiz);
        }
    }
    
}
